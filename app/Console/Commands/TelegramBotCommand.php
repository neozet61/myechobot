<?php

namespace App\Console\Commands;

use App\Service\ChatService;
use App\TelegramClientInterface;
use Illuminate\Console\Command;

class TelegramBotCommand extends Command
{
    protected ChatService $chatService;
    protected TelegramClientInterface $telegramClient;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:bot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for send messages from Telegram bot';

    public function __construct(ChatService $telegramBotService, TelegramClientInterface $telegramClient)
    {
        $this->chatService = $telegramBotService;
        $this->telegramClient = $telegramClient;
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $offset = 0;

        while (true) {
            $updates = $this->telegramClient->getUpdates($offset+1);

            foreach ($updates as $update) {
                //Записали обновление
                $offset = $update['update_id'];

                $this->chatService->handleUpdate($update);
            }
            sleep(1);
        }
    }
}
