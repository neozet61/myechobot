<?php

namespace App;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

class TelegramClient implements TelegramClientInterface
{
    protected string $telegramApiToken;
    protected ClientInterface $client;

    public function __construct(string $telegramApiToken, ClientInterface $client)
    {
        $this->telegramApiToken = $telegramApiToken;
        $this->client = $client;
    }

    /**
     * @throws GuzzleException
     */
    public function sendMessage(int $chatId, string $messageText): void
    {
        //Отправляем сообщение
        $this->client->request('POST', $this->getUrl('sendMessage'),
            [
                'form_params' => [
                    'chat_id' => $chatId,
                    'text' => $messageText,
                ]
            ]);
    }

    /**
     * @throws GuzzleException
     */
    public function getUpdates(int $offset): array
    {
        $response = $this->client->request('GET', $this->getUrl('getUpdates'), [
            "query" => [
                "offset" => $offset,
            ]
        ]);

        $bodyContents = $response->getBody()->getContents();

        $decodedBodyContents = json_decode($bodyContents, true);

        return $decodedBodyContents['result'];
    }

    protected function getUrl(string $method): string
    {
        return "https://api.telegram.org/bot{$this->telegramApiToken}/{$method}";
    }
}
