<?php

namespace App;

use Illuminate\Support\Facades\Log;

class FakeTelegramClient implements TelegramClientInterface
{
    public function sendMessage(int $chatId, string $messageText): void
    {
        Log::info("Отправили сообщение в chat_id={$chatId} c текстом {$messageText}");
    }
}
