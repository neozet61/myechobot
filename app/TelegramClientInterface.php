<?php

namespace App;

interface TelegramClientInterface
{
    public function sendMessage(int $chatId, string $messageText): void;
    public function getUpdates(int $offset): array;
}
