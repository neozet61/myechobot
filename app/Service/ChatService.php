<?php

namespace App\Service;

use App\TelegramClientInterface;
use GuzzleHttp\Exception\GuzzleException;

class ChatService
{
    protected TelegramClientInterface $telegramClient;

    /**
     * @param TelegramClientInterface $telegramClient
     */
    public function __construct(TelegramClientInterface $telegramClient)
    {
        $this->telegramClient = $telegramClient;
    }

    /**
     * @throws GuzzleException
     */
    public function handleUpdate($updateMassage): void
    {
        // Получаем чат ID
        $chatId = $updateMassage['message']['chat']['id'];

        // Получаем текст сообщения
        $messageText = $updateMassage['message']['text'] . "\nP.S. И кстати, классно выглядишь ;)";

        $this->telegramClient->sendMessage($chatId, $messageText);
    }
}
