<?php

namespace App\Providers;

use App\TelegramClient;
use App\TelegramClientInterface;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(TelegramClientInterface::class, function (){
            $apiToken = env('TELEGRAM_BOT_TOKEN');
            $client = new Client();
            return new TelegramClient($apiToken, $client);
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
