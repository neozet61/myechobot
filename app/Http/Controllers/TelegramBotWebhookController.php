<?php

namespace App\Http\Controllers;

use App\Service\ChatService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;

class TelegramBotWebhookController extends Controller
{
    public ChatService $chatService;

    public function __construct(ChatService $telegramBotService)
    {
        $this->chatService = $telegramBotService;
    }

    /**
     * @throws GuzzleException
     */
    public function handle(Request $request): void
    {
        $update = json_decode($request->getContent(), true);

        $this->chatService->handleUpdate($update);
    }
}
